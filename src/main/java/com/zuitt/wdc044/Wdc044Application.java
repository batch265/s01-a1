package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		// This method starts the whole Spring framework.
		SpringApplication.run(Wdc044Application.class, args);
	}

	// This is used for mapping HTTP GET requests.
	@GetMapping("/hello")
	// "@RequestParam" is used to extract query parameters, form parameters and even files from the request
		// name = john; Hello john
		// To append the URL with a name parameter followed by the "Key-Value" pairs
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s", name);
	}

	@GetMapping("/contactInfo")
	public String contactInfo (@RequestParam(value = "name", defaultValue = "User") String name, @RequestParam(value = "email", defaultValue = "user@mail.com") String email) {
		return  String.format("Hello %s! Your email is %s@mail.com.", name, email);
	}

	@GetMapping("/hi")
	public String hi (@RequestParam(value = "user", defaultValue = "user") String name) {
		return String.format("Hello %s!", name);
	}

	@GetMapping("/nameAge")
	public String nameAge (@RequestParam(value = "name", defaultValue = "You") String name, @RequestParam(value = "age", defaultValue = "(Your age in years)") String age) {
		return String.format("Hello %s! Your age is %s.", name, age);
	}


}

package com.zuitt.wdc044.models;

import javax.persistence.*;

@Entity
// declares that this particular java class will be the representation of the database table

@Table(name="posts")
// Designate table name via @Table
public class Post {

    // Indicates that this property represents the primary key via @Id
    @Id
    // values for this property will be auto-incremented
    @GeneratedValue
    private Long id;

    @Column
    // Declares the objects below as columns in the database
    private String title;

    @Column
    private String content;

    @ManyToOne
    //to reference the foreign key column
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public Post() {}

    public Post (String title, String content) {
//        this.user = user;
        this.title = title;
        this.content = content;
    }

    // GETTERs and SETTERs

    public String getTitle() {
        return  title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() { return user; }

    public void setUser(User user) {
        this.user = user;
    }




}

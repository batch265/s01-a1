package com.zuitt.wdc044.models;
// Abstraction, Controllers,
// jwt.secret is to check if password has been tampered.
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.ResponseEntity;

import javax.persistence.*;
import java.util.Set;

// Activity s02
    @Entity

    @Table(name = "users")
    public class User {

        @Id

        @GeneratedValue
        public Long id;

        @Column
        private String username;
        @Column
        private String password;

        //represents the one side of the relationship
        @OneToMany(mappedBy = "user")
        //to prevent the infinite recursion
        @JsonIgnore
        private Set<Post> posts;

    public User(){}

        public User(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public Long getId(){
            return id;
        }

        // Getters and Setters

        public String getUsername() {
            return username;
        }
        public void setUsername(String username){
            this.username = username;
        }

        public String getPassword() {
            return password;
        }
        public void setPassword(String password) {
            this.password = password;
        }

        public Set<Post> getPosts(){
            return posts;
        }

}


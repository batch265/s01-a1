package com.zuitt.wdc044.services;


import com.zuitt.wdc044.models.User;

import java.util.Optional;

// This interface will be used ti register a user via controller'
public interface UserService {
    // set methods but logic not logic

    // registration of a user
    void createUser(User user);

    // Check if user exists;
    Optional<User> findByUsername(String username);
}

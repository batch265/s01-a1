package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createPost (String stringToken, Post post) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);

        postRepository.save(newPost);
    }

    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    public ResponseEntity updatePost(Long id, String stringToken, Post post) {

        // Receive the post for updating using the "id" provided
        Post postForUpdating = postRepository.findById(id).get();

        // Because relationship is established between the User and Post model, we are able to retrieve the username of the post owner
        String postAuthor = postForUpdating.getUser().getUsername();

        //We will retrieve the username of the currently logged-in user in the token
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        //We will check if the currently logged-in user is the owner of the post
        if(authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());

            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this post", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForUpdating = postRepository.findById(id).get();

        String postAuthor = postForUpdating.getUser().getUsername();

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);

            return new ResponseEntity<>("Post deleted!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are now authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    // myPosts
    public ResponseEntity retrieveMyPosts(String stringToken) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return (ResponseEntity)author.getPosts();
//        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
//        return (ResponseEntity) author.getPosts();
//        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
//        if (authenticatedUser.equals(author)) {
////            return new ResponseEntity<>author.getPosts();
//            return (ResponseEntity) author.getPosts();
//        } else {
//            return new ResponseEntity<>("Something went wrong, input token again", HttpStatus.FAILED_DEPENDENCY);
//        }


    }




}

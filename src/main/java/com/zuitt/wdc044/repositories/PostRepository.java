package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository // Contains the records for Data Manipulation
public interface PostRepository extends CrudRepository<Post, Object> {

}
